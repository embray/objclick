objclick Changelog
==================

v0.1.2 (unreleased)
-------------------

* Nothing changed yet.


v0.1.1 (2020-10-02)
-------------------

### Bug fixes

* Fixed a bug with calling the callback of a `classcommand` or a
  `classgroup` via `super()` in a subclass.


v0.1.0 (2020-10-01)
-------------------

* Initial release.
