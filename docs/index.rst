.. objclick documentation master file, created by
   sphinx-quickstart on Thu Oct  1 15:52:15 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to objclick's documentation!
====================================

.. contents:: Table of Contents
    :local:
    :depth: 3


Introduction
------------

.. mdinclude:: ../README.md
    :start-line: 3


API documentation
-----------------

For more capabilities and examples, see the full API documentation:

.. toctree::
   :maxdepth: 3

   api


Changelog
---------

.. mdinclude:: ../CHANGELOG.md
    :start-line: 3


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
